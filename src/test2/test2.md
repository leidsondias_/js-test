# Teste 2
Neste teste é pedido para desenvolver um aplicativo para construir páginas com os seguintes requisitos.

# PARTE 1

O usuário terá uma toolbar na lateral esquerda da página, contento três seções para que o usuário possa arrastar para a página e construir o layout.

As seções conterão os seguintes componentes editáveis:

1º Seção: título, subtítulo e imagem (para imagem pode ser enviado apenas um link externo);
2º Seção: background (pode ser informado o hexa manualmente), título, subtítulo e um botão (podendo ser editado link, o target e o texto interno do botão);
3º Seção: a data de modificação (validar esta entrada) e três imagens, podendo ser informado uma, duas, três ou nenhuma imagem.

## Funcionamento

A. Cada item da seção poderá ser editado separadamente da forma que for mais conveniente.

B. Quando uma seção for arrastada para a "página"(container do layout do usuário), será adicionado ao JSON da página o objeto da seção adicionada, podendo o usuário adicionar quantas seções quiser ao layout.

C. As alterações nos dados tem que ser mostradas na lateral direita (Dados JSON) usando uma identação que torna a leitura do código JSON possível.

C. O usuário poderá ordenar as seções no layout depois de adicioná-las.

D. O usuário poderá deletar uma seção adicionada.

# PARTE 2

Antes de alterar os dados JSON da página, faça uma validação do mesmo, verificando se a estrutura não foi quebrada ou se os tipos de dados são diferente do esperado. Use o método que melhor se adequa.


# Observações

Será levado em conta quanto do teste foi concluído, sendo que se mais itens forem atentidos, mais relevante será considerado o teste.
